(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*global angular*/
/*global d3*/
'use strict';

var template = '' +
'<style>' +
'    .center {' +
'        text-align: center;' +
'    }' +
'</style>' +
'<div style="width: 410px" class="center">' +
'    <div ng-if="state === \'loading\'"><i class="fa fa-spinner fa-pulse fa-3x" aria-hidden="true"></i></div>' +
'    <div ng-if="state === \'failed\'">Could not load any data</div>' +
'    <div ng-if="state === \'success\'">' +
'        <div class="btn-group" role="group">' +
'            <button ng-click="$ctrl.changeDisplay(\'cpu\')" type="button" class="btn btn-default" translate>CPU</button>' +
'            <button ng-click="$ctrl.changeDisplay(\'memory\')" type="button" class="btn btn-default" translate>Memory</button>' +
'            <button ng-click="$ctrl.changeDisplay(\'disk\')" type="button" class="btn btn-default" translate>Disk</button>' +
'        </div>' +
'        <div ng-if="displayedGraph === \'cpu\'" class="graph">' +
'            <bitcraft-horizontal-pie-chart options="options[0]" data="data[0]"></bitcraft-horizontal-pie-chart>' +
'        </div>' +
'        <div ng-if="displayedGraph === \'memory\'" class="graph">' +
'            <bitcraft-horizontal-pie-chart options="options[1]" data="data[1]"></bitcraft-horizontal-pie-chart >' +
'        </div>' +
'        <div ng-if="displayedGraph === \'disk\'" class="graph">' +
'            <bitcraft-horizontal-pie-chart options="options[2]" data="data[2]"></bitcraft-horizontal-pie-chart >' +
'        </div>' +
'    </div>' +
'</div>' +
'';


//noinspection JSUnresolvedFunction
angular.module('bitcraft-hardware-monitoring').
    component('bitcraftHardwareMonitoring', {
        template: template,
        controller: ['$scope', '$stateParams', 'RestHttp', 'Notification', 'HardwareMonitoring',
            function ($scope, $stateParams, RestHttp, Notification, HardwareMonitoring) {
                var self = this;

                var refresh = function () {
                    var params;
                    var command = $stateParams.command;
                    var settings = HardwareMonitoring.getConfig();
                    if (settings[command] && settings[command].address && settings[command].port) {
                        params = {
                            address: self.coldata[settings[command].address] || settings[command].address,
                            port: self.coldata[settings[command].port] || settings[command].port
                        };
                    } else {
                        params = {
                            address: self.coldata.address,
                            port: self.coldata.port
                        };
                    }

                    if (settings[command] && settings[command].path) {
                        params.path = settings[command].path;
                    } else {
                        params.path = '/hw/basic';
                    }

                    RestHttp.restPost('getStats', params).then(function (data) {
                        $scope.timeout = setTimeout(refresh, 3000);
                        data = data.split(' ');
                        $scope.state = 'success';
                        console.log(data);
                        $scope.data[0][0].value = data[3];
                        $scope.data[0][1].value = data[1];
                        $scope.data[0][2].value = data[0];
                        $scope.data[1][0].value = data[4];
                        $scope.data[1][1].value = data[5];
                        $scope.data[1][2].value = data[6];
                        $scope.data[2][0].value = data[7];
                        $scope.data[2][1].value = 100 - data[7];
                    }, function (resp) {
                        $scope.state = 'failed';
                        Notification.error(
                            {message: 'Failed to retrieve statistics.' + ' (' + resp.statusText + ')'}
                        );
                    });
                };

                //noinspection JSUnusedGlobalSymbols
                this.$onDestroy = function () {
                    if ($scope.timeout) {
                        clearTimeout($scope.timeout);
                    }
                };

                this.$onInit = function () {
                    $scope.displayedGraph = 'cpu';
                    $scope.state = 'loading';
                    $scope.options = [
                        {
                            title: 'CPU',
                            width: 400,
                            height: 30,
                            valueSuffix: '%'
                        }, {
                            title: 'Memory',
                            width: 400,
                            height: 30,
                            valueSuffix: 'MB'
                        }, {
                            title: 'Disk',
                            width: 400,
                            height: 30,
                            valueSuffix: '%'
                        }
                    ];

                    $scope.data = [
                        [
                            {
                                legend: 'Idle',
                                value: 100
                            },
                            {
                                legend: 'System',
                                value: 0
                            }, {
                                legend: 'User',
                                value: 0
                            }
                        ], [
                            {
                                legend: 'Free',
                                value: 100
                            },
                            {
                                legend: 'Buffer/Cache',
                                value: 0
                            }, {
                                legend: 'Used',
                                value: 0
                            }
                        ], [
                            {
                                legend: 'Used',
                                value: 0
                            },
                            {
                                legend: 'Free',
                                value: 100
                            }
                        ]
                    ];

                    refresh();
                };

                /**
                 * @param {string} newDisplay
                 */
                this.changeDisplay = function (newDisplay) {
                    $scope.displayedGraph = newDisplay;
                };
            }],
        bindings: {
            coldata: '='
        }
    });


},{}],2:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-hardware-monitoring', ['bitcraft-horizontal-pie-chart']);

},{}],3:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

'use strict';
/*global angular*/
//noinspection JSUnresolvedFunction
angular.module('bitcraft-hardware-monitoring')
    .factory('HardwareMonitoring', [
        function () {
            var config = {};

            function getConfig() {
                return config;
            }

            function setConfig(newConfig) {
                config = newConfig;
            }

            return {
                getConfig: getConfig,
                setConfig: setConfig
            };
        }
    ]);

},{}],4:[function(require,module,exports){
'use strict';
require('./hardware-monitoring.module.js');
require('./hardware-monitoring.component.js');
require('./hardware-monitoring.service.js');

},{"./hardware-monitoring.component.js":1,"./hardware-monitoring.module.js":2,"./hardware-monitoring.service.js":3}]},{},[4]);
