/**
 * Created by richard on 19/10/16.
 */

'use strict';
/*global angular*/
//noinspection JSUnresolvedFunction
angular.module('bitcraft-hardware-monitoring')
    .factory('HardwareMonitoring', [
        function () {
            var config = {};

            function getConfig() {
                return config;
            }

            function setConfig(newConfig) {
                config = newConfig;
            }

            return {
                getConfig: getConfig,
                setConfig: setConfig
            };
        }
    ]);
