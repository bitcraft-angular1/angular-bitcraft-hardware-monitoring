/*global angular*/
/*global d3*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('bitcraft-hardware-monitoring').
    component('bitcraftHardwareMonitoring', {
        templateUrl: './hardware-monitoring.template.html',
        controller: ['$scope', '$stateParams', 'RestHttp', 'Notification', 'HardwareMonitoring',
            function ($scope, $stateParams, RestHttp, Notification, HardwareMonitoring) {
                var self = this;

                var refresh = function () {
                    var params;
                    var command = $stateParams.command;
                    var settings = HardwareMonitoring.getConfig();
                    if (settings[command] && settings[command].address && settings[command].port) {
                        params = {
                            address: self.coldata[settings[command].address] || settings[command].address,
                            port: self.coldata[settings[command].port] || settings[command].port
                        };
                    } else {
                        params = {
                            address: self.coldata.address,
                            port: self.coldata.port
                        };
                    }

                    if (settings[command] && settings[command].path) {
                        params.path = settings[command].path;
                    } else {
                        params.path = '/hw/basic';
                    }

                    RestHttp.restPost('getStats', params).then(function (data) {
                        $scope.timeout = setTimeout(refresh, 3000);
                        data = data.split(' ');
                        $scope.state = 'success';
                        console.log(data);
                        $scope.data[0][0].value = data[3];
                        $scope.data[0][1].value = data[1];
                        $scope.data[0][2].value = data[0];
                        $scope.data[1][0].value = data[4];
                        $scope.data[1][1].value = data[5];
                        $scope.data[1][2].value = data[6];
                        $scope.data[2][0].value = data[7];
                        $scope.data[2][1].value = 100 - data[7];
                    }, function (resp) {
                        $scope.state = 'failed';
                        Notification.error(
                            {message: 'Failed to retrieve statistics.' + ' (' + resp.statusText + ')'}
                        );
                    });
                };

                //noinspection JSUnusedGlobalSymbols
                this.$onDestroy = function () {
                    if ($scope.timeout) {
                        clearTimeout($scope.timeout);
                    }
                };

                this.$onInit = function () {
                    $scope.displayedGraph = 'cpu';
                    $scope.state = 'loading';
                    $scope.options = [
                        {
                            title: 'CPU',
                            width: 400,
                            height: 30,
                            valueSuffix: '%'
                        }, {
                            title: 'Memory',
                            width: 400,
                            height: 30,
                            valueSuffix: 'MB'
                        }, {
                            title: 'Disk',
                            width: 400,
                            height: 30,
                            valueSuffix: '%'
                        }
                    ];

                    $scope.data = [
                        [
                            {
                                legend: 'Idle',
                                value: 100
                            },
                            {
                                legend: 'System',
                                value: 0
                            }, {
                                legend: 'User',
                                value: 0
                            }
                        ], [
                            {
                                legend: 'Free',
                                value: 100
                            },
                            {
                                legend: 'Buffer/Cache',
                                value: 0
                            }, {
                                legend: 'Used',
                                value: 0
                            }
                        ], [
                            {
                                legend: 'Used',
                                value: 0
                            },
                            {
                                legend: 'Free',
                                value: 100
                            }
                        ]
                    ];

                    refresh();
                };

                /**
                 * @param {string} newDisplay
                 */
                this.changeDisplay = function (newDisplay) {
                    $scope.displayedGraph = newDisplay;
                };
            }],
        bindings: {
            coldata: '='
        }
    });

