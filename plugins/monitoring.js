'use strict';

// local
var exec = require('child_process').exec;
var http = require('http');

var m_returnCodes;// = require('laposte').returnCodes;
/** @type {Log4jsLogger} */
var m_logger;// = log4js.getLogger('monitoring');

/**
 *
 * @param {Log4jsLogger} logger
 * @param returnCodes
 */
function setup(logger, returnCodes) {
    m_logger = logger;
    m_returnCodes = returnCodes;
}

/**
 * Make a get HTTP request on address:port/path
 * @param {string} address
 * @param {string|number} port
 * @param {string} path
 * @param {function (Error, string)} callback
 */
function callWget(address, port, path, callback) {
    if (path[0] !== '/') {
        path = '/' + path;
    }
    var options = {
        hostname: address,
        port: port,
        path: path,
        method: 'GET'
    };

    var req = http.request(options, function (response) {
        var str = '';

        //another chunk of data has been received, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been received, so we just print it out here
        response.on('end', function () {
            callback(undefined, str);
        });
    });

    req.on('error', function (error) {
        callback(error);
    });

    req.end();
}

/**
 * Get statistics on the distant machine using http
 * @param {object} request
 * @param {object} request.data
 * @param {string} request.data.address
 * @param {number} request.data.port
 * @param {string} request.data.path
 * @param callback
 */
function getStats(request, callback) {
    if (!request || !request.data) {
        callback(m_returnCodes.INTERNAL_ERROR);
        return;
    }
    var data = request.data;
    if (!data.address || !data.port || !data.path) {
        callback(m_returnCodes.BAD_REQUEST);
        return;
    }

    callWget(data.address, data.port, data.path, function (error, string) {
        if (error) {
            m_logger.error('Failed to fetch: ' + error);
            callback(m_returnCodes.INTERNAL_ERROR);
            return;
        }

        callback(m_returnCodes.OK, string);
    });
}

module.exports = {
    plugins: {
        getStats: {
            method: 'POST',
            callback: getStats,
            anonymous: true,
            inContentType: 'application/json',
            outContentType: 'text/plain'
        }
    },
    setup: setup
};
