#!/usr/bin/env bash
function getCpu() {
    CPU_REPORT=`vmstat -n 1 2 | tail -1`
    CPU_USER=`echo $CPU_REPORT | awk '{ print $13 }'`
    CPU_SYSTEM=`echo $CPU_REPORT | awk '{ print $14 }'`
    CPU_IOWAIT=`echo $CPU_REPORT | awk '{ print $16 }'`
    CPU_IDLE=`echo $CPU_REPORT | awk '{ print $15 }'`

    echo "$CPU_USER $CPU_SYSTEM $CPU_IOWAIT $CPU_IDLE"
}

function getMemory() {
    FREE=`free -m | grep Mem | awk '{print $4}'`
    USED=`free -m | grep Mem | awk '{print $3}'`
    BUFF=`free -m | grep Mem | awk '{print $6}'`

    echo "$FREE $USED $BUFF"
}

function getDisk() {
    echo `df -P | grep -w "/" | awk '{ print $5 }' | cut -d% -f1`
}

function getDetailedCpu() {
    OLDIFS=$IFS
    IFS=$'\n'
    declare -a RAW_CPU_DATA_T
    declare -a RAW_CPU_DATA_TT

    # get current stat
    INDEX=0
    while read CPU_DATA; do 
        RAW_CPU_DATA_T[INDEX]="$CPU_DATA"
        ((INDEX++))
    done < <( cat /proc/stat | grep cpu )

    sleep 1

    # get stat 1 sec later
    INDEX=0
    while read CPU_DATA; do 
        RAW_CPU_DATA_TT[INDEX]="$CPU_DATA"
        ((INDEX++))
    done < <( cat /proc/stat | grep cpu )

    i=0
    while [ $i -lt ${#RAW_CPU_DATA_T[@]} ]; do
        SUM=0
        for j in {2..11}; do
            VAL_T=`echo ${RAW_CPU_DATA_T[$i]} | awk '{print $'$j'}'`
            VAL_TT=`echo ${RAW_CPU_DATA_TT[$i]} | awk '{print $'$j'}'`
            ((SUM+=($VAL_TT - $VAL_T)))
        done

        # name
        NEW_VALUE=`echo ${RAW_CPU_DATA_T[$i]} | awk '{print $1}'`

        # user  
        VAL_T=`echo ${RAW_CPU_DATA_T[$i]} | awk '{print $2}'`
        VAL_TT=`echo ${RAW_CPU_DATA_TT[$i]} | awk '{print $2}'`
        VAL_PERCENT=$(bc <<< "scale=2; x=100*($VAL_TT-$VAL_T)/$SUM; if(x<1 && x>0) print 0; x")
        NEW_VALUE="$NEW_VALUE $VAL_PERCENT%"

        # idle
        VAL_T=`echo ${RAW_CPU_DATA_T[$i]} | awk '{print $5}'`
        VAL_TT=`echo ${RAW_CPU_DATA_TT[$i]} | awk '{print $5}'`
        VAL_PERCENT=$(bc <<< "scale=2; x=100*($VAL_TT-$VAL_T)/$SUM; if(x<1 && x>0) print 0; x")
        NEW_VALUE="$NEW_VALUE $VAL_PERCENT%"

        # others
        VAL_PERCENT=0
        for j in 3 4 6 7 8 9 10 11; do
            VAL_T=`echo ${RAW_CPU_DATA_T[$i]} | awk '{print $'$j'}'`
            VAL_TT=`echo ${RAW_CPU_DATA_TT[$i]} | awk '{print $'$j'}'`
            VAL_PERCENT=$(bc <<< "scale=2; x=100*($VAL_TT-$VAL_T)/$SUM + $VAL_PERCENT; if(x<1 && x>0) print 0; x")
        done
        NEW_VALUE="$NEW_VALUE $VAL_PERCENT%"

        ((i++))
        echo $NEW_VALUE
    done
    IFS=$OLDIFS
}

# uncomment to debug
# set -x

if [ "$1" == "--cpu" ]; then
    getDetailedCpu
elif [ "$1" == "--memory" ]; then
    cat /proc/meminfo | egrep Mem
elif [ "$1" == "--disk" ]; then
    DISK=`getDisk`
    echo "Used: $DISK%"
else
    CPU=`getCpu`
    MEM=`getMemory`
    DISK=`getDisk`
    echo "$CPU $MEM $DISK"
fi
